package io.novajox.pocketgarden.plant

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

@RestController
@RequestMapping("/api/plants")
class PlantController {

    @Autowired
    private lateinit var plantRepository: PlantRepository


    val allPlants: List<Plant>
        @GetMapping
        get() = plantRepository.findAll()


    @GetMapping("/{id}")
    fun getPlant(@PathVariable(value = "id") plantId: Long?): ResponseEntity<Plant> {
        val plant = plantRepository.findOne(plantId) ?: return ResponseEntity.notFound().build()
        return ResponseEntity.ok(plant)
    }

    @PostMapping
    fun createPlant(@Valid @RequestBody plant: Plant): ResponseEntity<Plant> {
        val savePlant = plantRepository.save(plant)
        return ResponseEntity.ok(savePlant)
    }


    @PutMapping("/{id}")
    fun updatePlant(@PathVariable(value = "id") plantId: Long?, @Valid @RequestBody newPlant: Plant): ResponseEntity<Plant> {
        val plant = plantRepository.findOne(plantId) ?: return ResponseEntity.notFound().build()
        plant.title = newPlant.title
        val updatedPlant = plantRepository.save(plant)
        return ResponseEntity.ok(updatedPlant)
    }


    @DeleteMapping("/{id}")
    fun deletePlant(@PathVariable(value = "id") plantId:Long?) :ResponseEntity<Plant> {
        val plant = plantRepository.findOne(plantId) ?: return ResponseEntity.notFound().build()
        plantRepository.delete(plant)
        return ResponseEntity.ok().build()
    }
}
