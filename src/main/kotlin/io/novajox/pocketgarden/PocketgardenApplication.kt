package io.novajox.pocketgarden

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@EnableJpaAuditing
@SpringBootApplication
open class PocketgardenApplication

fun main(args: Array<String>) {
    SpringApplication.run(PocketgardenApplication::class.java, *args)
}
