package io.novajox.pocketgarden.user

import io.novajox.pocketgarden.plant.Plant
import io.novajox.pocketgarden.user.exception.UserNameAlreadyTakenException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/users")
class UserController {

    @Autowired
    lateinit var userRepository: UserRepository

    @GetMapping
    fun all() : List<User> {
        return userRepository.findAll()
    }

    @PostMapping
    fun create(@RequestBody @Valid user:User) : ResponseEntity<User> {
        this.userRepository.findByName(user.name).ifPresent(
                { throw UserNameAlreadyTakenException() })
        return ResponseEntity.ok(userRepository.save(user))
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable (value = "id") userId: Long):ResponseEntity<User> {
        val user = userRepository.findOne(userId) ?: return ResponseEntity.notFound().build()
        userRepository.delete(user)
        return ResponseEntity.ok().build()
    }

}