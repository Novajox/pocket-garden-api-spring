package io.novajox.pocketgarden.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNameAlreadyTakenException extends RuntimeException {

	public UserNameAlreadyTakenException() {
		super("This username is already taken sorry");
	}
}