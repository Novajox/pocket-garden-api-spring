package io.novajox.pocketgarden.user

import org.hibernate.validator.constraints.NotBlank
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.*

@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener::class)

class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id:Long? = null

    @NotBlank
    var name : String? = null
}